# Praca Dyplomowa

### ---------------- -!-> Polski <-!- ----------------

Zrealizowana przeze mnie praca magisterska - Projekt systemu sterowania dla manipulatora 5DOF z
wykorzystaniem ROS. Powstała w oparciu o stworzony w czasie projektu inżynierskiego robot (link do repozytorium: https://gitlab.com/Adrian_Winkler/praca-dyplomowa.git). 

Działanie układu pokazuje niniejszy film: 
https://drive.google.com/file/d/112Hxr6GwMyvbwhRtl4ls_bbuPNe-LFkB/view

Testy:
https://drive.google.com/file/d/1BYG6sjSXFko_tiJecuIMbGI6-OiE4axv/view

Gazebo:
https://drive.google.com/file/d/1wevoRIxMrlRH79DBPlQpNBj0POj1YufI/view

Link do tekstu pracy magisterskiej (GitLab nie pozwala na podgląd pliku z poziomu przeglądarki):
https://drive.google.com/file/d/1u-ssmDqls72tnOqenRijW4jTS0n_fc3F/view



### ---------------- -!-> English <-!- ----------------

Made by me Master Thesis Project with title - Control System Project of the 5DOF Manipulator Using
ROS. Work is based on my Engineering Thesis Project (link to repository: https://gitlab.com/Adrian_Winkler/praca-dyplomowa.git).

Here is link to short movie, how entire device is working:
https://drive.google.com/file/d/112Hxr6GwMyvbwhRtl4ls_bbuPNe-LFkB/view

Tests:
https://drive.google.com/file/d/1BYG6sjSXFko_tiJecuIMbGI6-OiE4axv/view

Gazebo:
https://drive.google.com/file/d/1wevoRIxMrlRH79DBPlQpNBj0POj1YufI/view

Link to my Master Thesis (GitLab do not allow to open it in browser):
https://drive.google.com/file/d/1u-ssmDqls72tnOqenRijW4jTS0n_fc3F/view