#!/usr/bin/env python3

#importowanie bibliotek    
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

# inicjalizacja wezla ROS
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

# wczytywanie modelu robota
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group = moveit_commander.MoveGroupCommander("Arm")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory)

# pobranie aktualnego stanu robota
group_variable_values = group.get_current_joint_values()
group_tolerance_values = group.get_goal_position_tolerance()

# nowe nastawy robota
group_variable_values[0] = 0
group_variable_values[1] = 0
group_variable_values[2] = -0.2
group_variable_values[4] = 1

# ustawienie tolerancji
group_tolerance_values += 0.05

# wpisanie nowych nastaw robota
group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

waypoints = []

# dodanie punktow do trasy robota
print(group.get_current_pose().pose)
waypoints.append(group.get_current_pose().pose)
wpose = geometry_msgs.msg.Pose()
wpose.position.x = waypoints[0].position.x
wpose.position.y = waypoints[0].position.y + 0.03
wpose.position.z = waypoints[0].position.z
waypoints.append(copy.deepcopy(wpose))
waypoints.pop(0)

# wyznaczenie trajektorii
(plan3, fraction) = group.compute_cartesian_path(
                             waypoints,   
                             0.01,        
                             0.0)        

# wykonanie trasy
group.execute(plan3, wait=True)


# odczekanie 5 sekund
rospy.sleep(5)


waypoints = []
# dodanie punktow do trasy robota
print(group.get_current_pose().pose)
waypoints.append(group.get_current_pose().pose)
wpose = geometry_msgs.msg.Pose()
wpose.position.x = waypoints[0].position.x
wpose.position.y = waypoints[0].position.y 
wpose.position.z = waypoints[0].position.z + 0.02
waypoints.append(copy.deepcopy(wpose))
waypoints.pop(0)

# wyznaczenie trajektorii
(plan3, fraction) = group.compute_cartesian_path(
                             waypoints,
                             0.01,        
                             0.0)         

# wykonanie trasy
group.execute(plan3, wait=True)

# odczekanie 5 sekund
rospy.sleep(5)


waypoints = []
# dodanie punktow do trasy robota
print(group.get_current_pose().pose)
waypoints.append(group.get_current_pose().pose)
wpose = geometry_msgs.msg.Pose()
wpose.position.x = waypoints[0].position.x
wpose.position.y = waypoints[0].position.y -0.03
wpose.position.z = waypoints[0].position.z
waypoints.append(copy.deepcopy(wpose))
waypoints.pop(0)


# wyznaczenie trajektorii
(plan3, fraction) = group.compute_cartesian_path(
                             waypoints,
                             0.01,        
                             0.0)         
# wykonanie trasy
group.execute(plan3, wait=True)

# odczekanie 5 sekund
rospy.sleep(5)

waypoints = []

# dodanie punktow do trasy robota
print(group.get_current_pose().pose)
waypoints.append(group.get_current_pose().pose)
wpose = geometry_msgs.msg.Pose()
wpose.position.x = waypoints[0].position.x
wpose.position.y = waypoints[0].position.y 
wpose.position.z = waypoints[0].position.z - 0.02
waypoints.append(copy.deepcopy(wpose))
waypoints.pop(0)

# wyznaczenie trajektorii
(plan3, fraction) = group.compute_cartesian_path(
                             waypoints,
                             0.01,       
                             0.0)        

# wykonanie trasy
group.execute(plan3, wait=True)

# odczekanie 5 sekund
rospy.sleep(5)

