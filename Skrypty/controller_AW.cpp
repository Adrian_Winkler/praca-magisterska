#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>


// Klasa ta zasadniczo nie czyni nic, musi po prostu byc obecna by ROS nie zglaszal bledu o braku ukladu wykonawczego. 
// W przypadku gdy jest uruchomiona symulacja Gazebo nie jest wymagana
class MyRobot : public hardware_interface::RobotHW
{
public:
  MyRobot() 
 { 
   hardware_interface::JointStateHandle state_handle_1("base_link__link_01", &pos[0], &vel[0], &eff[0]);
   jnt_state_interface.registerHandle(state_handle_1);

   hardware_interface::JointStateHandle state_handle_2("link_01__link_02", &pos[1], &vel[1], &eff[1]);
   jnt_state_interface.registerHandle(state_handle_2);

   hardware_interface::JointStateHandle state_handle_3("link_02__link_03", &pos[2], &vel[2], &eff[2]);
   jnt_state_interface.registerHandle(state_handle_3);

   hardware_interface::JointStateHandle state_handle_4("link_03__link_04", &pos[3], &vel[3], &eff[3]);
   jnt_state_interface.registerHandle(state_handle_4);

   hardware_interface::JointStateHandle state_handle_5("link_04__link_05", &pos[4], &vel[4], &eff[4]);
   jnt_state_interface.registerHandle(state_handle_5);

   registerInterface(&jnt_state_interface);

   hardware_interface::JointHandle pos_handle_1(jnt_state_interface.getHandle("base_link__link_01"), &cmd[0]);
   jnt_pos_interface.registerHandle(pos_handle_1);

   hardware_interface::JointHandle pos_handle_2(jnt_state_interface.getHandle("link_01__link_02"), &cmd[1]);
   jnt_pos_interface.registerHandle(pos_handle_2);

   hardware_interface::JointHandle pos_handle_3(jnt_state_interface.getHandle("link_02__link_03"), &cmd[2]);
   jnt_pos_interface.registerHandle(pos_handle_3);

   hardware_interface::JointHandle pos_handle_4(jnt_state_interface.getHandle("link_03__link_04"), &cmd[3]);
   jnt_pos_interface.registerHandle(pos_handle_4);

   hardware_interface::JointHandle pos_handle_5(jnt_state_interface.getHandle("link_04__link_05"), &cmd[4]);
   jnt_pos_interface.registerHandle(pos_handle_5);


   registerInterface(&jnt_pos_interface);
  }

private:
  hardware_interface::JointStateInterface jnt_state_interface;
  hardware_interface::PositionJointInterface jnt_pos_interface;
  double cmd[5];
  double pos[5];
  double vel[5];
  double eff[5];
};


main()
{
  // Stworznie obiektu klasy MyRobot
  MyRobot robot;
  controller_manager::ControllerManager cm(&robot);

  // Nieskonczona petla
  while (true)
  {
     robot.read();
     cm.update(robot.get_time(), robot.get_period());
     robot.write();
     sleep();
  }
}

