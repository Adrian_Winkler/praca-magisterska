#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/hw_timer.h"
#include "driver/adc.h"
#include "esp_task_wdt.h"

#define LINE 2

/* Definiowanie zmiennych */

// Enkoder 1
uint8_t counter1 = 0;
int position = 0;
int last_position = 0;
unsigned char direction;
unsigned char state;

// Enkoder 2
uint8_t counter2 = 0;
int position2 = 0;
int last_position2 = 0;
unsigned char direction2;
unsigned char state2;

// Enkoder 3
uint8_t counter3 = 0;
int position3 = 0;
int last_position3 = 0;
unsigned char direction3;
unsigned char state3;

// Enkoder 4
uint8_t counter4 = 0;
int position4 = 0;
int last_position4 = 0;
unsigned char direction4;
unsigned char state4;

// Enkoder 5
uint8_t counter5 = 0;
bool position5 = false;
bool last_position5 = false;
unsigned char direction5;
unsigned char state5;

// Dzielniki rozdzielczosci i ograniczenia
uint8_t steps_per_click = 1;
uint16_t upper_bound = 30536;
int16_t lower_bound = -30536;

#define RE_RIGHT            1
#define RE_LEFT             255


// Funkcja dzielaca rozdzielczosc enkoderow
int getPosition() {
  return position / steps_per_click;
}


// Watek 1
void vTask1function(void* pvParameters)
{
   
    uint8_t cnt1 = 0;
    while (1) 
    {
      // Petla sprawdza zmiane stanu enkodera co 10 ms
      vTaskDelay(10 / portTICK_RATE_MS);
      esp_task_wdt_reset();
    
      int s = state & 3;
      // Odczyt stanu pinow do ktorych jest podloczony enkoder 1
      if (gpio_get_level(4)) s |= 4;
      if (gpio_get_level(12)) s |= 8;

      // Maszyna stanow niwelujaca problem bouncingu
      switch (s) {
        case 0: case 5: case 10: case 15:
          break;
        case 1: case 7: case 8: case 14:
            position++; break;
        case 2: case 4: case 11: case 13:
          position--; break;
        case 3: case 12:
          position += 2; break;
        default:
          position -= 2; break;
      }
      state = (s >> 2);
      
      // Zmiana wartosci licznika enkodera
      if (getPosition() >= lower_bound && getPosition() <= upper_bound) {
        if (position != last_position) {
          if (abs(position - last_position) >= steps_per_click) {
            last_position = position;      
          }      
        }
      } else position = last_position;
      if(cnt1%10 == 0){
        ets_printf("C%d\n", position);
      }

      ++cnt1;       
       
    }

}

// Watek 2
void vTask2function(void* pvParameters)
{
    uint8_t cnt2 = 0;
    vTaskDelay(1 / portTICK_RATE_MS);
    while (1) 
    {
      // Petla sprawdza zmiane stanu enkodera co 10 ms
      vTaskDelay(10 / portTICK_RATE_MS);
  
      int s2 = state2 & 3;

      // Odczyt stanu pinow do ktorych jest podloczony enkoder 2
      if (gpio_get_level(5)) s2 |= 4;
      if (gpio_get_level(14)) s2 |= 8;

      // Maszyna stanow niwelujaca problem bouncingu
      switch (s2) {
        case 0: case 5: case 10: case 15:
          break;
        case 1: case 7: case 8: case 14:
            position2++; break;
        case 2: case 4: case 11: case 13:
          position2--; break;
        case 3: case 12:
          position2 += 2; break;
        default:
          position2 -= 2; break;
      }
      state2 = (s2 >> 2);

      // Zmiana wartosci licznika enkodera
      if (getPosition() >= lower_bound && getPosition() <= upper_bound) {
        if (position2 != last_position2) {
          if (abs(position2 - last_position2) >= steps_per_click) {
            last_position2 = position2;      
          }   
        }
      } else position2 = last_position2;

      // Cykliczne wyslanie stanu enkodera 
      if(cnt2%10 == 0){
        ets_printf("D%d\n", position2);
      }

      ++cnt2;          
  }
}

// Watek 3
void vTask3function(void* pvParameters)
{
    uint8_t cnt3 = 0;
    vTaskDelay(2 / portTICK_RATE_MS);
    while (1) 
    {
    
      // Petla sprawdza zmiane stanu enkodera co 10 ms
      vTaskDelay(10 / portTICK_RATE_MS);
    
      int s3 = state3 & 3;
      // Odczyt stanu pinow do ktorych jest podloczony enkoder 3
      if (gpio_get_level(16)) s3 |= 4;
      if (gpio_get_level(13)) s3 |= 8;

      // Maszyna stanow niwelujaca problem bouncingu
      switch (s3) {
        case 0: case 5: case 10: case 15:
          break;
        case 1: case 7: case 8: case 14:
            position3++; break;
        case 2: case 4: case 11: case 13:
          position3--; break;
        case 3: case 12:
          position3 += 2; break;
        default:
          position3 -= 2; break;
      }
      state3 = (s3 >> 2);

      // Zmiana wartosci licznika enkodera
      if (getPosition() >= lower_bound && getPosition() <= upper_bound) {
        if (position3 != last_position3) {
          if (abs(position3 - last_position3) >= steps_per_click3) {
            last_position3 = position3;      
          }
        }
      } else position3 = last_position3;

      // Cykliczne wyslanie stanu enkodera
      if(cnt3%10 == 0){
        ets_printf("A%d\n", position3);
      }

      ++cnt3;             
    }
}


// Watek 4
void vTask4function(void* pvParameters)
{
    uint8_t cnt4 = 0;
    vTaskDelay(3 / portTICK_RATE_MS);
    while (1) 
    {
      // Petla sprawdza zmiane stanu enkodera co 10 ms
      vTaskDelay(10 / portTICK_RATE_MS); 

      int s4 = state4 & 3;
      // Odczyt stanu pinow do ktorych jest podloczony enkoder 4
      if (gpio_get_level(2)) s4 |= 4;
      if (gpio_get_level(15)) s4 |= 8;

      // Maszyna stanow niwelujaca problem bouncingu
      switch (s4) {
        case 0: case 5: case 10: case 15:
          break;
        case 1: case 7: case 8: case 14:
            position4++; break;
        case 2: case 4: case 11: case 13:
          position4--; break;
        case 3: case 12:
          position4 += 2; break;
        default:
          position4 -= 2; break;
      }
      state4 = (s4 >> 2);
      
      // Zmiana wartosci licznika enkodera
      if (getPosition() >= lower_bound && getPosition() <= upper_bound) {
        if (position4 != last_position4) {
          if (abs(position4 - last_position4) >= steps_per_click3) {
            last_position4 = position4;      
          }     
        }
      } else position4 = last_position4;

      // Cykliczne wyslanie stanu enkodera 
      if(cnt4%10 == 0){
        ets_printf("B%d\n", position4);
      }

      ++cnt4;    
  }
}

// Watek 5
void vTask5function(void* pvParameters){
        uint8_t cnt_mod = 0;
        vTaskDelay(30 / portTICK_RATE_MS);
        last_position5 = gpio_get_level(3);

   while (1) 
    {
      // Petla sprawdza zmiane stanu enkodera co 40 ms
      vTaskDelay(40 / portTICK_RATE_MS);
        position5 = gpio_get_level(3);
        if ( position5 != last_position5){
          counter5 += 1;
          if(counter5%2 == 0){
          ets_printf("E1\n");
          fflush(stdout);
            counter5 = 0;
          }
          last_position5 = position5;
        }
    }
}



// Glowna petla programu
void app_main(void)
{
    // Ustawienie pinow GPIO mikrokontrolera
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_INPUT;
    /* Pin 5 -> D1, Pin 4 -> D2, Pin 14 -> D5, Pin 12 -> D6, Pin 13 -> D7, Pin 0 -> D3, */
    io_conf.pin_bit_mask = ((1<<4) | (1<<5) | (1<<12) | (1<<14) | (1<<13) | (1<<16) | (1<<2) | (1<<3) | (1<<15));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);
    ets_printf("Encoder start!\n");

    // Uruchomienie watkow
    xTaskCreate(vTask1function, "TASK 1", 1024*1, NULL, 1, NULL );
    xTaskCreate(vTask2function, "TASK 2", 1024*1, NULL, 1, NULL );
    xTaskCreate(vTask3function, "TASK 3", 1024*1, NULL, 1, NULL );
    xTaskCreate(vTask4function, "TASK 4", 1024*1, NULL, 1, NULL );
    xTaskCreate(vTask5function, "TASK 5", 1024*1, NULL, 2, NULL );
}





