#!/usr/bin/env python3

# sudo chmod 666 /dev/ttyUSB0
# sudo chmod 666 /dev/ttyACM0

import rospy
from std_msgs.msg import String
import sensor_msgs.msg
import numpy as np
import serial
import time
import moveit_msgs.msg
from queue import Queue
from threading import Thread, Lock, Event
from control_msgs.msg import FollowJointTrajectoryActionFeedback

rospy.init_node('talker', anonymous=True)



# funkcja publikujaca stany enkoderow
def talker(in_q1, in_q2, in_q_dir, lock, eve):
    global motor1_p
    global motor2_p
    global motor3_p
    global motor4_p 
    global motor5_p
    dir = 0.0
    seq_tal = []
    ser_tal = serial.Serial('/dev/ttyUSB0', 74880)
    cnt_list = 0
    clock = 0
    flaga = 0
    prev_cnt = 0.0
    axis_1 = 0
    axis_1p = 0
    axis_2 = 0
    axis_2p = 0
    axis_3 = 0
    axis_3p = 0
    axis_4 = 0
    axis_4p = 0
    axis_5 = 0
    axis_5p = 0
    compense1 = 0
    compense2 = 0
    gripper_pose = 0
    dir_enc = 0
    cnt_diff = 0
    pub = rospy.Publisher(
        'joint_states', sensor_msgs.msg.JointState, queue_size=10)
    # nieskonczony nasluch portu szeregowego
    while not rospy.is_shutdown():
        for c in ser_tal.read(1):
            seq_tal.append(chr(c))
            cnt_list += 1
            if (chr(c) == '\n'):
                string = ''.join(seq_tal[1:-1])
                cnt_diff += 1
                if (seq_tal[0] == 'A'):
                    axis_1 = int(string)*1.5
                elif (seq_tal[0] == 'C'):
                    axis_4 = -int(string)
                elif (seq_tal[0] == 'D'):
                    axis_3 = int(string)
                elif (seq_tal[0] == 'B'):
                    axis_2 = -int(string)
                    if (not in_q1.empty()):
                        gripper_pose = in_q1.get()
                    if (not in_q2.empty()):
                        compense2 = in_q2.get()
                    if (not in_q_dir.empty()):
                        dir_enc = in_q_dir.get()
                    msg = sensor_msgs.msg.JointState()
                    msg.header.stamp = rospy.Time.now()
                    msg.name.append('base_link__link_01')
                    msg.name.append('link_01__link_02')
                    msg.name.append('link_02__link_03')
                    msg.name.append('link_03__link_04')
                    msg.name.append('link_04__link_05')
                    msg.name.append('link_05__link_06')
                    msg.name.append('link_05__link_07')
                    msg.position = [np.deg2rad(axis_1), np.deg2rad(axis_2*0.14), np.deg2rad(axis_3*0.3214)-(compense1*0.05),   np.deg2rad(
                        axis_4*0.4) - (compense2*0.83), np.deg2rad(axis_5), gripper_pose, gripper_pose]  # /48/14  48/15 36/25
                    msg.velocity = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                    msg.effort = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                    pub.publish(msg)

                elif (seq_tal[0] == 'E'):
                    axis_5 = axis_5 + dir_enc
                cnt_list = 0
                seq_tal = []
                if (cnt_diff % 20 == 0):
                    if (axis_1 == axis_1p and axis_2 == axis_2p and axis_3 == axis_3p and axis_4 == axis_4p and axis_5 == axis_5p):
                        lock.acquire()
                        motor1_p = np.deg2rad(axis_4*0.4) - (compense2*0.83)
                        motor2_p = np.deg2rad(axis_5)
                        motor3_p = np.deg2rad(axis_3*0.3214)
                        motor4_p = np.deg2rad(axis_2*0.14)
                        motor5_p = np.deg2rad(axis_1)
                        lock.release()
                        eve.set()
                    cnt_diff = 0
                    axis_1p = axis_1
                    axis_2p = axis_2
                    axis_3p = axis_3
                    axis_4p = axis_4
                    axis_5p = axis_5
                else:
                    eve.clear()

# funkcja wywolywana w czasie otrzymania rozkazu przemieszczenia osi
def callback(data, out_q1, out_q2, out_q_dir, lock, eve):
    global motor1_p
    global motor2_p
    global motor3_p
    global motor4_p
    global motor5_p
    global dir_5
    global dir_4
    global dir_3
    global dir_2
    global dir_1
    global cnt
    global ser
    # sprawdzanie czy otrzymana wiadomosc przemieszczenia dotyczy chwytaka
    if (data.goal.trajectory.joint_trajectory.joint_names[0] == 'link_05__link_06'):
        gripper = -0.229
        gripper_duty = 1
        # ustalenie poziomu odchylenia chwytaka do wykonania
        while (gripper < data.goal.trajectory.joint_trajectory.points[-1].positions[0]):
            gripper += 0.04
            gripper_duty += 1
            # wyslanie wiadomosci do mikrokontrolera STM by ten uruchomil chwytak
        message_str = str(0) + str(0).zfill(4) + '6' + str(0) + str(0).zfill(4) + '6' + str(0) + str(0).zfill(
            4) + '6' + str(0) + str(0).zfill(4) + '6' + str(0) + str(0).zfill(4) + '61' + str(gripper_duty)
        message = message_str.encode()
        ser.write(str.encode(message_str))
        rospy.loginfo(rospy.get_caller_id() + 'I heard %s', message)
        out_q1.put(
            data.goal.trajectory.joint_trajectory.points[-1].positions[0])
    else:
        # synchronizacja miedzy watkami
        lock.acquire()
        motor1_p = data.goal.trajectory.joint_trajectory.points[0].positions[3]
        motor2_p = data.goal.trajectory.joint_trajectory.points[0].positions[4]
        motor3_p = data.goal.trajectory.joint_trajectory.points[0].positions[2]
        motor4_p = data.goal.trajectory.joint_trajectory.points[0].positions[1]
        motor5_p = data.goal.trajectory.joint_trajectory.points[0].positions[0]
        lock.release()
        # wyznaczenie ilosci kroku do wykonania przez poszczegulne silniki
        for point in (data.goal.trajectory.joint_trajectory.points):
         #           rospy.loginfo(rospy.get_caller_id() + "I heard %s", point.positions)

            motor2 = int(
                np.round(np.abs((np.abs(point.positions[4]) - np.abs(motor2_p))*37250/3.14)))
            if (point.positions[4]*motor2_p < 0):
                a = 0
            elif ((point.positions[4] > 0) and motor2_p > 0):
                if ((point.positions[4] - motor2_p) > 0):
                    dir_2 = 2
                    out_q_dir.put(1)
                else:
                    dir_2 = 1
                    out_q_dir.put(-1)
            else:
                if ((point.positions[4] - motor2_p) < 0):
                    dir_2 = 1
                    out_q_dir.put(-1)
                else:
                    dir_2 = 2
                    out_q_dir.put(1)
            lock.acquire()
            motor5 = (point.positions[0]) - (motor5_p)
            lock.release()
            if (motor5 < 0):
                dir_5 = 1

            else:
                dir_5 = 2

            motor5 = int(np.round(abs(motor5*32250/3.14)))

            motor4 = (point.positions[1]) - (motor4_p)
            compense4 = motor4*14/25
            if (motor4 < 0):
                dir_4 = 1
            else:
                dir_4 = 2
            motor4 = int(np.round(np.abs(motor4*56250/3.14)))

            motor3 = point.positions[2] - motor3_p  # + compense4
#            motor3 = motor3*0.8
            compense3 = motor3*0.53
            if (motor3 < 0):
                dir_3 = 1
            else:
                dir_3 = 2
            motor3 = int(np.round(np.abs(motor3*22250/3.14)))

            motor1 = point.positions[3] - (motor1_p) + (compense3)
            if (motor1 < 0):
                dir_1 = 2
            else:
                dir_1 = 1
            motor1 = int(np.round(np.abs(motor1*17500/3.14)))
#            out_q1.put(point.positions[1])
            out_q2.put(point.positions[2])
            print('A1: ' + str(motor5) + ' A2: ' + str(motor4) + ' A3: ' +
                  str(motor3) + ' A4: ' + str(motor1) + ' A5: ' + str(motor2))
            message_str = str(dir_1) + str(motor1).zfill(4) + '6' + str(dir_2) + str(motor2).zfill(4) + '6' + str(dir_3) + str(
                motor3).zfill(4) + '6' + str(dir_4) + str(motor4).zfill(4) + '6' + str(dir_5) + str(motor5).zfill(4) + '600'
            message = message_str.encode()
            ser.write(str.encode(message_str))
#            rospy.loginfo(rospy.get_caller_id() + 'I heard %s', message)
            lock.acquire()
            motor1_p = point.positions[3]
            motor2_p = point.positions[4]
            motor3_p = point.positions[2]
            motor4_p = point.positions[1]
            motor5_p = point.positions[0]
            lock.release()
            cnt = 0
            time.sleep(0.1)
        flag_axis_0 = 1
        while flag_axis_0:
            time.sleep(3)
            eve.wait()
            lock.acquire()
            motor1 = (
                (data.goal.trajectory.joint_trajectory.points[-1].positions[3]) - (motor1_p))
            motor2 = - \
                ((data.goal.trajectory.joint_trajectory.points[-1].positions[4]) - (
                    motor2_p))
            motor3 = (
                (data.goal.trajectory.joint_trajectory.points[-1].positions[2]) - (motor3_p))
            motor4 = (
                (data.goal.trajectory.joint_trajectory.points[-1].positions[1]) - (motor4_p))
            motor5 = (
                (data.goal.trajectory.joint_trajectory.points[-1].positions[0]) - (motor5_p))
            print('A1: ' + str(motor5) + ' A2: ' + str(motor4) + ' A3: ' +
                  str(motor3) + ' A4: ' + str(motor1) + ' A5: ' + str(motor2))
            if np.abs(motor1) < 0.02:
                motor1 = 0
            if np.abs(motor2) < 0.02:
                motor2 = 0
            if np.abs(motor3) < 0.02:
                motor3 = 0
            if np.abs(motor4) < 0.02:
                motor4 = 0
            if np.abs(motor5) < 0.02 and motor1 == motor2 == motor3 == motor4:
                motor5 = 0
                flag_axis_0 = 0
            lock.release()
            motor2 = int(
                np.round(np.abs((np.abs(point.positions[4]) - np.abs(motor2_p))*37250/3.14)))
            if (point.positions[4]*motor2_p < 0):
                a = 0
            elif ((point.positions[4] > 0) and motor2_p > 0):
                if ((point.positions[4] - motor2_p) > 0):
                    dir_2 = 2
                    out_q_dir.put(1)
                else:
                    dir_2 = 1
                    out_q_dir.put(-1)
            else:
                if ((point.positions[4] - motor2_p) < 0):
                    dir_2 = 1
                    out_q_dir.put(-1)
                else:
                    dir_2 = 2
                    out_q_dir.put(1)
            lock.acquire()
            motor5 = (point.positions[0]) - (motor5_p)
            lock.release()
            if (motor5 < 0):
                dir_5 = 1

            else:
                dir_5 = 2

            motor5 = int(np.round(abs(motor5*32250/3.14)))

            motor4 = (point.positions[1]) - (motor4_p)
            compense4 = motor4*14/25
            if (motor4 < 0):
                dir_4 = 1
            else:
                dir_4 = 2
            motor4 = int(np.round(np.abs(motor4*56250/3.14)))

            motor3 = point.positions[2] - motor3_p  # + compense4
#            motor3 = motor3*0.8
            compense3 = motor3*0.53
            if (motor3 < 0):
                dir_3 = 1
            else:
                dir_3 = 2
            motor3 = int(np.round(np.abs(motor3*22250/3.14)))

            motor1 = point.positions[3] - (motor1_p) + (compense3)
            if (motor1 < 0):
                dir_1 = 2
            else:
                dir_1 = 1
            motor1 = int(np.round(np.abs(motor1*17500/3.14)))
#            out_q1.put(point.positions[1])
            out_q2.put(point.positions[2])
            # wyslanie wiadomosci do mikrokontrolera STM 
            message_str = str(dir_1) + str(motor1).zfill(4) + '6' + str(dir_2) + str(motor2).zfill(4) + '6' + str(dir_3) + str(
                motor3).zfill(4) + '6' + str(dir_4) + str(motor4).zfill(4) + '6' + str(dir_5) + str(motor5).zfill(4) + '600'
            message = message_str.encode()
            ser.write(str.encode(message_str))
#    cnt = cnt + 1

# funkcja nasluchujaca rozkazow poruszenia osiami
def listener(out_q1, out_q2, out_q_dir, lock, eve):
    def callback_lambda(x): return callback(
        x, out_q1, out_q2, out_q_dir, lock, eve)
    rospy.Subscriber('/execute_trajectory/goal',
                     moveit_msgs.msg.ExecuteTrajectoryActionGoal, callback_lambda)

    rospy.spin()


if __name__ == '__main__':
    # tworzenie zmiennych globalnych
    motor1_p = 0
    motor2_p = 0
    motor3_p = 0
    motor4_p = 0
    motor5_p = 0
    dir_5 = 0
    dir_5_p = -1
    dir_4 = 0
    dir_3 = 0
    dir_2 = 0
    dir_1 = 0
    ser = serial.Serial('/dev/ttyACM0', 115200)

    # definiowanie kolejek do komunikacji między watkami
    q1 = Queue()
    q2 = Queue()
    q_dir = Queue()
    # definiowanie zmiennych synchronizujących komunikacje miedzy watkami
    lock = Lock()
    event = Event()
    # tworzenie watkow
    t1 = Thread(target=talker, args=(q1, q2, q_dir, lock, event))
    t2 = Thread(target=listener, args=(q1, q2, q_dir, lock, event))
    try:
        t1.start()
        t2.start()
    except rospy.ROSInterruptException:
        pass
