#!/usr/bin/env python3
    
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import numpy as np

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group = moveit_commander.MoveGroupCommander("Arm")
group_grip = moveit_commander.MoveGroupCommander("Gripper")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory)

group_variable_values = group.get_current_joint_values()
group_tolerance_values = group.get_goal_position_tolerance()

group_grip_variable_values = group_grip.get_current_joint_values()
group_grip_tolerance_values = group_grip.get_goal_position_tolerance()

# -------------------- -!-> Robot open gripper <-!- --------------------

group_grip_variable_values[0] = -0.23
group_grip_variable_values[1] = -0.23

group_grip_tolerance_values += 0.01

group_grip.set_joint_value_target(group_grip_variable_values)
group_grip.set_goal_position_tolerance(group_grip_tolerance_values)


plan2 = group_grip.plan()
group_grip.go(wait=True)

rospy.sleep(1)

# -------------------- -!-> Robot go down <-!- --------------------

AX_0 = 60

group_variable_values[0] = np.deg2rad(5)
group_variable_values[1] = np.deg2rad(-3)
group_variable_values[2] = np.deg2rad(-37)
group_variable_values[3] = np.deg2rad(0)
group_variable_values[4] = np.deg2rad(0)

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan() 
print(plan2)  
group.go(wait=True)

# -------------------- -!-> Robot grip element <-!- --------------------

rospy.sleep(0.5)

group_grip_variable_values[0] = -0.08
group_grip_variable_values[1] = -0.08

group_grip_tolerance_values += 0.01

group_grip.set_joint_value_target(group_grip_variable_values)
group_grip.set_goal_position_tolerance(group_grip_tolerance_values)

plan2 = group_grip.plan()
group_grip.go(wait=True)

# -------------------- -!-> Robot go up <-!- --------------------

rospy.sleep(0.5)

group_variable_values[0] = 0
group_variable_values[1] = 0
group_variable_values[2] = 0
group_variable_values[3] = 0
group_variable_values[4] = 0

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan()
group.go(wait=True)


# -------------------- -!-> Robot turn right <-!- --------------------

rospy.sleep(0.5)



group_variable_values[0] = np.deg2rad(AX_0)
group_variable_values[1] = 0
group_variable_values[2] = 0
group_variable_values[3] = 0
group_variable_values[4] = 0

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan()
group.go(wait=True)

# -------------------- -!-> Robot go down right <-!- --------------------

rospy.sleep(0.5)


group_variable_values[0] = np.deg2rad(AX_0)
group_variable_values[1] = np.deg2rad(-5)
group_variable_values[2] = np.deg2rad(-35)
group_variable_values[3] = np.deg2rad(0)
group_variable_values[4] = np.deg2rad(0)

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan()


group.go(wait=True)

# -------------------- -!-> Robot open gripper <-!- --------------------

rospy.sleep(0.5)

group_grip_variable_values[0] = -0.23
group_grip_variable_values[1] = -0.23

group_grip_tolerance_values += 0.01

group_grip.set_joint_value_target(group_grip_variable_values)
group_grip.set_goal_position_tolerance(group_grip_tolerance_values)

plan2 = group_grip.plan()
group_grip.go(wait=True)


# -------------------- -!-> Robot go up <-!- --------------------

rospy.sleep(1)

group_variable_values[0] = np.deg2rad(AX_0)
group_variable_values[1] = 0
group_variable_values[2] = 0
group_variable_values[3] = 0
group_variable_values[4] = 0

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan()
group.go(wait=True)

# -------------------- -!-> Robot go home <-!- --------------------

rospy.sleep(0.5)

group_variable_values[0] = 0
group_variable_values[1] = 0
group_variable_values[2] = 0
group_variable_values[3] = 0
group_variable_values[4] = 0

group_tolerance_values += 0.02

group.set_joint_value_target(group_variable_values)
group.set_goal_position_tolerance(group_tolerance_values)

plan2 = group.plan()
group.go(wait=True)


# -------------------- -!-> Robot gripper close <-!- --------------------

rospy.sleep(1)

group_grip_variable_values[0] = -0.08
group_grip_variable_values[1] = -0.08

group_grip_tolerance_values += 0.01

group_grip.set_joint_value_target(group_grip_variable_values)
group_grip.set_goal_position_tolerance(group_grip_tolerance_values)

plan2 = group_grip.plan()
group_grip.go(wait=True)
