#!/usr/bin/env python3

import rospy
import time
from std_msgs.msg import String
import numpy as np
import sensor_msgs.msg

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from control_msgs.msg import FollowJointTrajectoryActionFeedback
import moveit_msgs.msg

import csv
 
cnt = 0

# funkcja wywolywana w momencie odebrania topicu /joint_states
def callback(data, group):
    global cnt
    cnt += 1
    if(cnt%15!=0):
        return
    if (True or data.goal.trajectory.joint_trajectory.joint_names[0] != 'link_05__link_06'):    
        file = open('/home/adrian/1_ROS/TEST_7/pomiary_pose.csv', 'a+', newline ='')
        timer = 0.0
        ACCU = 4
        pose_vector = []   
        pa = group.get_current_pose().pose
        # dodanie pozycji i orientacji robota do zmiennej
        pose_curr = [np.round(pa.position.x, ACCU), np.round(pa.position.y, ACCU), np.round(pa.position.z, ACCU), 
                    np.round(pa.orientation.x, ACCU), np.round(pa.orientation.y, ACCU), np.round(pa.orientation.z, ACCU)]
        pose_vector.append(pose_curr)
        # wpisanie zmiennej pose_vector do pliku csv
        with file:   
            write = csv.writer(file)
            write.writerows(pose_vector)

    
def listener():

      # zaladowanie modelu robota
      group = moveit_commander.MoveGroupCommander("Arm")
      callback_data = lambda x: callback(x, group)
      # inicjalizacja nowego wezla ROS
      rospy.init_node('listener_AW', anonymous=True)
      # w momencie odebrania topicu nastepuje wywolanie funkcji callback_data
      rospy.Subscriber('/joint_states', sensor_msgs.msg.JointState, callback_data)
      rospy.spin()
  
if __name__ == '__main__':
 
    listener()
    