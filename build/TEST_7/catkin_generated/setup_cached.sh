#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/adrian/1_ROS/TEST_7/devel/.private/TEST_7:$CMAKE_PREFIX_PATH"
export PWD='/home/adrian/1_ROS/TEST_7/build/TEST_7'
export ROSLISP_PACKAGE_DIRECTORIES="/home/adrian/1_ROS/TEST_7/devel/.private/TEST_7/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/adrian/1_ROS/TEST_7/src/TEST_7:$ROS_PACKAGE_PATH"