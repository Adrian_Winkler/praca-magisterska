import kdl_parser_py.urdf as kdl_parser
import PyKDL as kdl
import tf2_ros
import geometry_msgs.msg
import rospy

(status, tree) = kdl_parser.treeFromFile("/home/adrian/1_ROS/TEST_7/src/TEST_7/config/gazebo_mrm.urdf")

chain = tree.getChain("base_link", "link_05")
num_joints = chain.getNrOfJoints()
# print("\n*** This robot has %s joints *** \n" % num_joints)


fk_pos_solver = kdl.ChainFkSolverPos_recursive(chain)
ee_pose = kdl.Frame()
jointAngles=kdl.JntArray(5)
jointAngles[3] = 1.0
# theta = create_joint_angles([0, 0, 0, -1.57, 0, 1.57, 0])
fk_pos_solver.JntToCart(jointAngles, ee_pose)
# print("\n*** End-effector Position FK: ***")
vel_tim = [ee_pose.p.x(), ee_pose.p.y(),ee_pose.p.z(),]
print(vel_tim)
# print("\n*** Rotational Matrix FK: ***")
# print(ee_pose.M)