import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
from mpl_toolkits.mplot3d import Axes3D

# schnell_CHOMP = [0.00, -0.00, -0.00, -0.01, -0.01, -0.02, -0.03, -0.03, -0.04, -0.05, -0.06, -0.07, -0.08, -0.09, -0.09, -0.10, -0.11, -0.12, -0.13, -0.14, -0.14, -0.16, -0.17, -0.18, -0.19, -0.20, -0.21, -0.22, -0.23, -0.24, -0.25, -0.26, -0.27, -0.28, -0.29, -0.30, -0.31, -0.31, -0.32, -0.33, -0.34, -0.35, -0.35, -0.36, -0.37, -0.37, -0.38, -0.39, -0.40, -0.40, -0.41, -0.40, -0.39, -0.39, -0.38, -0.37, -0.36, -0.36, -0.35, -0.34, -0.34, -0.33, -0.32, -0.31, -0.30, -0.29, -0.29, -0.28, -0.27, -0.26, -0.25, -0.24, -0.23, -0.22, -0.21, -0.20, -0.19, -0.18, -0.16, -0.15, -0.14, -0.14, -0.13, -0.12, -0.11, -0.10, -0.09, -0.08, -0.07, -0.06, -0.06, -0.05, -0.04, -0.03, -0.03, -0.02, -0.01, -0.01, -0.00, -0.00, 0.00]
# zeit_CHOMP = [0.00, 0.01, 0.02, 0.04, 0.05, 0.07, 0.09, 0.11, 0.13, 0.15, 0.17, 0.19, 0.22, 0.24, 0.26, 0.29, 0.31, 0.34, 0.36, 0.39, 0.42, 0.44, 0.47, 0.50, 0.53, 0.55, 0.58, 0.61, 0.64, 0.66, 0.69, 0.72, 0.75, 0.77, 0.80, 0.83, 0.85, 0.88, 0.91, 0.93, 0.96, 0.99, 1.01, 1.04, 1.06, 1.09, 1.12, 1.14, 1.16, 1.19, 1.21, 1.24, 1.26, 1.29, 1.31, 1.34, 1.36, 1.39, 1.41, 1.44, 1.47, 1.49, 1.52, 1.55, 1.57, 1.60, 1.63, 1.66, 1.68, 1.71, 1.74, 1.77, 1.79, 1.82, 1.85, 1.88, 1.90, 1.93, 1.96, 1.99, 2.02, 2.04, 2.07, 2.10, 2.12, 2.15, 2.17, 2.20, 2.22, 2.24, 2.27, 2.29, 2.31, 2.33, 2.35, 2.37, 2.39, 2.40, 2.42, 2.43, 2.44]

# schnell_OMPL = [-0.00, -0.35, -0.35, -0.00]
# zeit_OMPL = [0.00, 0.82, 1.17, 1.99]

# schnell_PTP = [0.00, -0.05, -0.10, -0.15, -0.20, -0.25, -0.30, -0.35, -0.40, -0.45, -0.50, -0.47, -0.42, -0.37, -0.32, -0.27, -0.22, -0.17, -0.12, -0.07, -0.02, 0.00]
# zeit_PTP = [0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50, 1.60, 1.70, 1.80, 1.90, 2.00, 2.03]

# schnell_LIN = [-0.00, -0.02, -0.07, -0.11, -0.16, -0.19, -0.19, -0.19, -0.19, -0.18, -0.18, -0.18, -0.18, -0.17, -0.17, -0.17, -0.17, -0.16, -0.16, -0.16, -0.15, -0.15, -0.15, -0.15, -0.14, -0.14, -0.14, -0.14, -0.13, -0.13, -0.13, -0.12, -0.12, -0.12, -0.10, -0.07, -0.05, -0.02]
# zeit_LIN = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7]


pose1 = pd.read_csv("pomiary4.csv") 
pose2 = pd.read_csv("pomiary1.csv") 
pose3 = pd.read_csv("pomiary2.csv") 
pose4 = pd.read_csv("pomiary3.csv") 

schnell_CHOMP = pose2['CHOMP_V'].values
zeit_CHOMP = pose2['CHOMP_T'].values


schnell_OMPL = pose1['OMPL_V'].values
zeit_OMPL = pose1['OMPL_T'].values


schnell_PTP = pose3['PTP_V'].values
zeit_PTP = pose3['PTP_T'].values

schnell_LIN = pose4['LIN_V'].values
zeit_LIN = pose4['LIN_T'].values


v_CHOMP = [zeit_CHOMP[i+1]-zeit_CHOMP[i] for i in range(len(zeit_CHOMP)-2)]
v_OMPL = [zeit_OMPL[i+1]-zeit_OMPL[i] for i in range(len(zeit_OMPL)-2)]
v_PTP = [zeit_PTP[i+1]-zeit_PTP[i] for i in range(len(zeit_PTP)-2)]
v_LIN = [zeit_LIN[i+1]-zeit_LIN[i] for i in range(len(zeit_LIN)-2)]

print("MIN CHOMP:"+ str(np.min(v_CHOMP)))
print("MAX CHOMP:" + str(np.max(v_CHOMP)))    
print("MIN OMPL:"+ str(np.min( v_OMPL)))
print("MAX OMPL:" + str(np.max(v_OMPL)))    
print("MIN PTP:"+ str(np.min( v_PTP)))
print("MAX PTP:" + str(np.max(v_PTP)))    
print("MIN LIN:"+ str(np.min( v_LIN)))
print("MAX LIN:" + str(np.max(v_LIN)))    


plt.scatter(zeit_CHOMP, schnell_CHOMP, linewidth=6)
#plt.ylabel('Prędkość ramienia drugiego [rad/s]')
#plt.xlabel('Czas [s]')
#plt.grid('on')
#plt.title('Przebieg prędkości ramienia drugiego w czasie')

plt.scatter(zeit_OMPL, schnell_OMPL, marker="s", linewidth=15)

plt.scatter(zeit_PTP, schnell_PTP, marker="p", linewidth=8)

plt.scatter(zeit_LIN, schnell_LIN, marker="D", linewidth=2)


plt.ylabel('Prędkość ramienia drugiego [rad/s]')
plt.xlabel('Czas [s]')
plt.grid('on')
plt.title('Porównanie przebiegu prędkości ramienia drugiego (przemieszczenie 90 stopni) w czasie dla różnych plannerów')

plt.legend(['CHOMP', 'OMPL (BFMT)', 'PTP', 'LIN'], loc='lower right')


plt.show()