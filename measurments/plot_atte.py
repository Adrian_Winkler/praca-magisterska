import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


CHOMP_1 = [0.313, 0.285,  0.263, 0.253, 0.255, 0.255, 0.259, 0.266, 0.264, 0.257, 0.265]
BFMT_1 = [1.023, 0.991, 1.007, 0.967, 0.996, 1.149, 1.003, 1.164, 0.959, 1.010]
BiEST_1 =  [0.037, 0.038, 0.042, 0.044, 0.038, 0.045, 0.042, 0.048, 0.047, 0.040]
BiTRRT_1 = [0.029, 0.021, 0.025, 0.021, 0.023, 0.022, 0.023, 0.020, 0.026, 0.025, 0.021]
EST_1 =    [0.095, 0.103, 0.101, 0.108, 0.144, 0.070, 0.144, 0.138, 0.119, 0.124, 0.185]
FMT_1 =    [0.903, 0.949, 0.965, 0.933, 0.910, 0.892, 0.892, 0.964, 0.936, 0.875, 0.953]
KPIECE_1 = [0.136, 0.139, 0.129, 0.172, 0.157, 0.270, 0.109, 0.134, 0.138, 0.111, 0.165, 0.197]
PDST_1 =   [0.180, 0.131, 0.148, 0.228, 0.133, 0.155, 0.117, 0.100, 0.137, 0.114, 0.071]
PRM_1 =   [0.087, 0.080, 0.100, 0.080, 0.087, 0.073, 0.082, 0.091, 0.057, 0.119]
ProjEST_1 = [0.136, 0.163, 0.087, 0.190, 0.123, 0.164, 0.105, 0.064, 0.156, 0.149, 0.197]
RRTConnect_1 = [0.037, 0.033, 0.036, 0.035, 0.033, 0.031, 0.022, 0.046, 0.049, 0.034]
RRT_1 = [0.052, 0.074, 0.050, 0.040, 0.080, 0.072, 0.046, 0.041, 0.037, 0.044, 0.032]
STRIDE_1 = [0.123, 0.107, 0.132, 0.092, 0.081, 0.150, 0.087, 0.125, 0.106, 0.175, 0.125]
TRRT_1 = [0.039, 0.034, 0.022, 0.047, 0.032, 0.035, 0.029, 0.030, 0.035, 0.030]
LIN_1 =  [0.007, 0.004, 0.006, 0.005, 0.007, 0.0004, 0.004, 0.006, 0.004, 0.006, 0.007, 0.005]
PTP_1 =  [0.001, 0.000, 0.001, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.001 ]


CHOMP_5 = [0.290, 0.284, 0.275, 0.272, 0.289, 0.288, 0.284, 0.279, 0.304, 0.273, 0.286]
BFMT_5 = [1.216, 1.272, 1.222, 1.378, 1.347, 1.206, 1.221, 1.224, 1.231, 1.404, 1.239]
BiEST_5 =  [0.045, 0.038, 0.036, 0.044, 0.034, 0.066, 0.037, 0.042, 0.038, 0.036]
BiTRRT_5 = [0.039, 0.042, 0.032, 0.038, 0.042, 0.038, 0.043, 0.053, 0.039, 0.037, 0.039]
EST_5 =    [0.078, 0.123, 0.098, 0.219, 0.170, 0.124, 0.094, 0.112, 0.163, 0.078, 0.081]
FMT_5 =    [2.512, 2.455, 2.751, 2.868, 2.551, 2.582, 2.842, 2.423, 2.614, 2.643, 2.460]
KPIECE_5 = [0.228, 0.292, 0.090, 0.082, 0.062, 0.241, 0.110, 0.111, 0.097, 0.108]
PDST_5 =   [0.121, 0.099, 0.113, 0.131, 0.073, 0.134, 0.081, 0.091, 0.119, 0.137, 0.171]
PRM_5 =   [0.083, 0.089, 0.115, 0.084, 0.098, 0.091, 0.085, 0.089, 0.071, 0.087]
ProjEST_5 = [0.101, 0.128, 0.105, 0.175, 0.078, 0.130, 0.125, 0.113, 0.098, 0.102]
RRTConnect_5 = [0.030, 0.030, 0.029, 0.032, 0.026, 0.027, 0.027, 0.033, 0.031, 0.029]
RRT_5 = [0.055, 0.075, 0.049, 0.066, 0.053, 0.068, 0.045, 0.057, 0.060, 0.057, 0.046]
STRIDE_5 = [0.099, 0.154, 0.079, 0.106, 0.138,  0.128, 0.169, 0.161, 0.100, 0.122]
TRRT_5 = [0.096, 0.068, 0.069, 0.072, 0.097, 0.082, 0.067, 0.058, 0.081, 0.058]
LIN_5=  [0.008, 0.009, 0.013, 0.010, 0.013, 0.011, 0.006, 0.011, 0.013, 0.010, 0.011]
PTP_5 =  [0.001, 0.000, 0.001, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.001] 



CHOMP_1A = [0.248, 0.249, 0.244, 0.239, 0.249, 0.251, 0.246, 0.241, 0.247, 0.245]
# CHOMP_20A = [0.252, 0.247, 0.250, 0.245, 0.245, 0.246, 0.251, 0.252, 0.248, 0.248]
BFMT_1A = [0.227, 0.219, 0.218, 0.234, 0.238, 0.212, 0.208, 0.203, 0.222, 0.226, 0.2236]
# BFMT_20A =  [2.106, 2.195, 2.162,  2.167, 2.103, 2.122, 2.115, 2.112, 2.165, 2.112, 2.096]
BiEST_1A =  [0.020, 0.025, 0.022, 0.029, 0.021, 0.017, 0.017, 0.025, 0.021]
# BiEST_20A = [0.053, 0.052, 0.047, 0.062, 0.053, 0.050, 0.057]
BiTRRT_1A =  [0.024, 0.025, 0.024, 0.022, 0.020, 0.024, 0.025]
# BiTRRT_20A = [0.048, 0.054, 0.049, 0.049, 0.052, 0.049, 0.051, 0.053]
EST_1A =  [0.051, 0.0018, 0.068, 0.032,0.021, 0.037, 0.028, 0.020]
# EST_20A = [0.155, 0.145, 0.255, 0.227, 0.118,  0.211, 0.207, 0.213, 0.198]
KPIECE_1A =  [0.042, 0.023, 0.022, 0.027, 0.097, 0.034, 0.033, 0.076, 0.066]
# KPIECE_20A = [0.216, 0.253, 0.229, 0.158, 0.216, 0.277, 0.277, 0.215]
PDST_1A =   [0.039, 0.023, 0.024, 0.017, 0.033, 0.032]
# PDST_20A =   [0.170, 0.148, 0.134, 0.207, 0.141, 0.187, 0.223, 0.202]
PRM_1A =  [0.022, 0.024, 0.019, 0.017, 0.014, 0.016]
# PRM_20A =  [0.159, 0.162, 0.135, 0.139, 0.185, 0.167]
RRTConnect_1A =  [0.018, 0.024, 0.027, 0.018, 0.024, 0.024]
# RRTConnect_20A = [0.039, 0.042, 0.038, 0.037, 0.040, 0.043, 0.046, 0.045]
RRT_1A =  [0.020, 0.023, 0.019, 0.039, 0.032, 0.019, 0.018]
# RRT_20A = [0.082, 0.088, 0.091, 0.101, 0.078, 0.088, 0.076, 0.083, 0.092]
STRIDE_1A =  [0.029, 0.022, 0.038, 0.035, 0.023, 0.029]
# STRIDE_20A =  [0.264, 0.194, 0.195, 0.227, 0.197, 0.277, 0.171, 0.182]
TRRT_1A =  [0.021, 0.021, 0.025, 0.023, 0.028, 0.022]
# TRRT_20A = [0.119, 0.109, 0.119, 0.130, 0.104, 0.124, 0.113]


EST_20A = [0.155, 0.145, 0.255, 0.227, 0.118,  0.211, 0.207, 0.213, 0.198]
CHOMP_20A = [ 0.254, 0.247,  0.250, 0.250, 0.252,  0.250, 0.243, 0.248, 0.260]
BFMT_20A =  [10.483, 10.528,  10.374, 11.494, 10.487, 11.796, 10.675 ]
BiEST_20A = [0.204, 0.194, 0.212, 0.202, 0.190, 0.197, 0.203, 0.184]
BiTRRT_20A = [0.219, 0.239, 0.251,  0.222, 0.241, 0.232,  0.225, 0.248, 0.223, 0.244]
KPIECE_20A = [ 2.008, 3.007, 1.949, 1.923, 2.857, 2.699, 2.919, 1.659 ]
PDST_20A =   [0.827, 1.023,  0.882, 0.889, 0.919,  0.919, 0.719, 0.767, 0.646, 0.865]
PRM_20A =  [0.683, 0.685, 0.611, 0.627, 0.769, 0.681, 0.750, 0.662   ]
RRTConnect_20A = [0.130, 0.140, 0.140, 0.137, 0.135, 0.133 , 0.137, 0.138  ] 
RRT_20A = [0.376, 0.378, 0.382, 0.420, 0.439, 0.402, 0.381, 0.453  ]
STRIDE_20A =  [ 0.927, 1.095,  0.969, 0.980, 0.895, 0.945, 0.910, 1.109]
TRRT_20A = [0.532, 0.575, 0.577,  0.551, 0.551, 0.634,  0.579, 0.510, 0.540  ]




CH1 = np.round(np.mean(CHOMP_1A), 3)
CH5 = np.round(np.mean(CHOMP_5), 3)
CH20 = np.round(np.mean(CHOMP_20A), 3)

BF1 = np.round(np.mean(BFMT_1A), 3)
BF5 = np.round(np.mean(BFMT_5), 3)
BF20 = np.round(np.mean(BFMT_20A), 3)

Bi1 = np.round(np.mean(BiEST_1A), 3)
Bi5 = np.round(np.mean(BiEST_5), 3)
Bi20 = np.round(np.mean(BiEST_20A), 3)


BiT1 = np.round(np.mean(BiTRRT_1A), 3)
BiT5 = np.round(np.mean(BiTRRT_5), 3)
BiT20 = np.round(np.mean(BiTRRT_20A), 3)

EST1 = np.round(np.mean(EST_1A), 3)
EST5 = np.round(np.mean(EST_5), 3)
EST20 = np.round(np.mean(EST_20A), 3)

KPI1 = np.round(np.mean(KPIECE_1A), 3)
KPI5 = np.round(np.mean(KPIECE_5), 3)
KPI20 = np.round(np.mean(KPIECE_20A), 3)

PRM1 = np.round(np.mean(PRM_1), 3)
PRM5 = np.round(np.mean(PRM_5), 3)
PRM20 = np.round(np.mean(PRM_5), 3)

PDS1 = np.round(np.mean(PDST_1), 3)
PDS5 = np.round(np.mean(PDST_5), 3)
PDS20 = np.round(np.mean(PDST_5), 3)

RRTC1 = np.round(np.mean(RRTConnect_1A), 3)
RRTC5 = np.round(np.mean(RRTConnect_5), 3)
RRTC20 = np.round(np.mean(RRTConnect_20A), 3)

RRT1 = np.round(np.mean(RRT_1A), 3)
RRT5 = np.round(np.mean(RRT_5), 3)
RRT20 = np.round(np.mean(RRT_20A), 3)

STR1 = np.round(np.mean(STRIDE_1A), 3)
STR5 = np.round(np.mean(STRIDE_5), 3)
STR20 = np.round(np.mean(STRIDE_20A), 3)

TRRT1 = np.round(np.mean(TRRT_1A), 3)
TRRT5 = np.round(np.mean(TRRT_5), 3)
TRRT20 = np.round(np.mean(TRRT_20A), 3)




labels = ['CHOMP', 'BFMT', 'BiEST', 'BiTRRT', 'EST', 'KPIECE', 'PRM', 'PDST', 'RRTC', 'RRT', 'STRIDE', 'TRRT']
pla_1_means = [CH1, BF1, Bi1, BiT1, EST1, KPI1, PRM1, PDS1, RRTC1, RRT1, STR1, TRRT1]
pla_5_means = [CH5, BF5, Bi5, BiT5, EST5, KPI5, PRM5, PDS5, RRTC5, RRT5, STR5, TRRT5]
pla_20_means = [CH20, BF20, Bi20, BiT20, EST20, KPI20, PRM20, PDS20, RRTC20, RRT20, STR20, TRRT20]


x = np.arange(len(labels))  # the label locations
width = 0.20  # the width of the bars

# print(len(labels))
# print(len(pla_1_means))

# fig, ax = plt.subplots()
# ax.grid(zorder=0)
# rects1 = ax.bar(x - width, pla_1_means, width, label='1 podejście', color=['fuchsia'], zorder=3)
# rects2 = ax.bar(x, pla_5_means, width, label='10 podejść', color=['springgreen'],zorder=3 )
# rects3 = ax.bar(x + width, pla_20_means, width, label='20 podejść', color=['blue'], zorder=3 )

# # Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Czas [s]')
# ax.set_title('Porównanie czasów planowania dla przypadków ruchu jednej i wszystkich osi')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
# ax.legend()



# def autolabel1(rects):
#     """Attach a text label above each bar in *rects*, displaying its height."""
#     for rect in rects:
#         height = rect.get_height()
#         ax.annotate('{}'.format(height),
#                     xy=(rect.get_x() + rect.get_width() / 2, height),
#                     xytext=(0, 3),  # 3 points vertical offset
#                     textcoords="offset points",
#                     ha='center', va='bottom')


# def autolabel2(rects):
#     for rect in rects:
#         height = rect.get_height()
#         ax.annotate('{}'.format(height),
#                     xy=(rect.get_x() + rect.get_width() / 2, height+0.02),
#                     xytext=(0, 3),  # 3 points vertical offset
#                     textcoords="offset points",
#                     ha='center', va='bottom')

# def autolabel3(rects):
#     for rect in rects:
#         height = rect.get_height()
#         ax.annotate('{}'.format(height),
#                     xy=(rect.get_x() + rect.get_width() / 2, height+0.04),
#                     xytext=(0, 3),  # 3 points vertical offset
#                     textcoords="offset points",
#                     ha='center', va='bottom')


# autolabel1(rects1)

# autolabel1([rects2[0], rects3[0], rects2[1], rects2[2], rects2[3],
#             rects2[4], rects3[4], rects2[5], rects3[5], rects2[9], rects3[9],
#             rects2[10], rects3[10], rects2[11], rects3[11], rects3[6], rects2[7]])


# autolabel2([rects3[7], rects3[1], rects3[2], rects3[3], rects2[6], rects2[8]])
# autolabel3([rects3[8]])
# # autolabel2(rects2)
# # autolabel3(rects3)

# fig.tight_layout()


# plt.show()






# XX = pd.Series(pla_20_means,index=labels)

fig, (ax1,ax2) = plt.subplots(2,1,sharex=True,
                         figsize=(5,6))
ax1.spines['bottom'].set_visible(False)
ax1.tick_params(axis='x',which='both',bottom=False)
ax2.spines['top'].set_visible(False)

bs = 2.5
ts = 10

ax2.set_ylim(0,bs)
ax1.set_ylim(ts,11)
ax1.set_yticks(np.arange(10.25,11,0.25))

# bars1 = ax1.bar(XX.index, XX.values)
# bars2 = ax2.bar(XX.index, XX.values)

x = np.arange(len(labels))  # the label locations
width = 0.20  # the width of the bars

# print(len(labels))
# print(len(pla_1_means))

ax1.grid(zorder=0)
ax2.grid(zorder=0)
rects11 = ax1.bar(x - width, pla_1_means, width, label='1 podejście', color=['fuchsia'], zorder=3)
rects12 = ax2.bar(x - width, pla_1_means, width, label='1 podejście', color=['fuchsia'], zorder=3)
rects21 = ax1.bar(x, pla_5_means, width, label='10 podejść', color=['springgreen'],zorder=3 )
rects22 = ax2.bar(x, pla_5_means, width, label='10 podejść', color=['springgreen'],zorder=3 )
rects31 = ax1.bar(x + width, pla_20_means, width, label='100 podejść', color=['blue'], zorder=3 )
rects32 = ax2.bar(x + width, pla_20_means, width, label='100 podejść', color=['blue'], zorder=3 )



ax2.set_ylabel('Czas [s]')
ax1.set_ylabel('Czas [s]')
ax2.set_xlabel('Planner')
ax1.set_title('Porównanie czasów planowania dla przypadków ruchu jednej i wszystkich osi')
ax1.set_xticks(x)
ax2.set_xticks(x)
ax2.set_xticklabels(labels)


for tick in ax2.get_xticklabels():
    tick.set_rotation(0)
d = .008  
kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False)
ax1.plot((-d, +d), (-d, +d), **kwargs)      
ax1.plot((1 - d, 1 + d), (-d, +d), **kwargs)
kwargs.update(transform=ax2.transAxes)  
ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  
ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)

for b1, b2 in zip(rects11, rects12):
    posx = b2.get_x() + b2.get_width()/2.
    if b2.get_height() > bs:
        ax2.plot((posx-3*d, posx+3*d), (1 - d, 1 + d), color='k', clip_on=False,
                 transform=ax2.get_xaxis_transform())
    if b1.get_height() > ts:
        ax1.plot((posx-3*d, posx+3*d), (- d, + d), color='k', clip_on=False,
                 transform=ax1.get_xaxis_transform())

for b1, b2 in zip(rects21, rects22):
    posx = b2.get_x() + b2.get_width()/2.
    if b2.get_height() > bs:
        ax2.plot((posx-3*d, posx+3*d), (1 - d, 1 + d), color='k', clip_on=False,
                 transform=ax2.get_xaxis_transform())
    if b1.get_height() > ts:
        ax1.plot((posx-3*d, posx+3*d), (- d, + d), color='k', clip_on=False,
                 transform=ax1.get_xaxis_transform())

for b1, b2 in zip(rects31, rects32):
    posx = b2.get_x() + b2.get_width()/2.
    if b2.get_height() > bs:
        ax2.plot((posx-3*d, posx+3*d), (1 - d, 1 + d), color='k', clip_on=False,
                 transform=ax2.get_xaxis_transform())
    if b1.get_height() > ts:
        ax1.plot((posx-3*d, posx+3*d), (- d, + d), color='k', clip_on=False,
                 transform=ax1.get_xaxis_transform())                 





def autolabel1(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax2.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


def autolabel2(rects):
    for rect in rects:
        height = rect.get_height()
        ax2.annotate('{}'.format(height),

                    xy=(rect.get_x() + rect.get_width() / 2, height+0.04),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

def autolabel3(rects):
    for rect in rects:
        height = rect.get_height()
        ax2.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height+0.08),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

def autolabel31(rects):
    for rect in rects:
        height = rect.get_height()
        ax1.annotate('{}'.format(height),

                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel1(rects12)

autolabel1([rects32[0], rects22[1],
            rects22[4], rects32[4], rects22[5], rects32[5], rects32[9],
            rects22[10],rects32[10],rects32[11],rects32[6], rects32[7]])


autolabel2([rects22[2], rects22[0], rects32[2], rects32[3], rects22[9],rects22[11]])
autolabel3([ rects32[8],rects22[6], rects22[7], rects22[3], rects22[8]])
autolabel31([rects31[1]])


ax1.legend()


plt.show()