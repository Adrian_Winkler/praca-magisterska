import matplotlib
import matplotlib.pyplot as plt
import numpy as np


#Dla Goal Bias 0.5
EST_5 =    [0.025, 0.022, 0.026,  0.023, 0.024, 0.028, 0.021, 0.026, 0.024,  0.021, 0.016]
KPIECE_5 = [0.024, 0.035, 0.036,  0.021, 0.029, 0.022, 0.035, 0.024, 0.022, 0.027]
ProjEST_5 = [ 0.024, 0.018, 0.027,  0.024, 0.023, 0.019, 0.018, 0.018, 0.020, 0.026 ]
RRT_5 = [ 0.016, 0.022, 0.027, 0.022, 0.020, 0.023,  0.023, 0.019, 0.020,  0.017  ]
STRIDE_5 = [0.017, 0.020, 0.023,  0.026, 0.023, 0.025, 0.024, 0.026, 0.024, 0.029, 0.024]
TRRT_5 = [0.024, 0.032, 0.041, 0.029, 0.028, 0.030, 0.027, 0.031, 0.023, 0.028]

#Dla Goal Bias 0.05
EST_05 =    [0.078, 0.123, 0.098, 0.219, 0.170, 0.124, 0.094, 0.112, 0.163, 0.078, 0.081]
KPIECE_05 = [0.228, 0.292, 0.090, 0.082, 0.062, 0.241, 0.110, 0.111, 0.097, 0.108]
ProjEST_05 = [0.101, 0.128, 0.105, 0.175, 0.078, 0.130, 0.125, 0.113, 0.098, 0.102]
RRT_05 = [0.055, 0.075, 0.049, 0.066, 0.053, 0.068, 0.045, 0.057, 0.060, 0.057, 0.046]
STRIDE_05 = [0.099, 0.154, 0.079, 0.106, 0.138,  0.128, 0.169, 0.161, 0.100, 0.122]
TRRT_05 = [0.096, 0.068, 0.069, 0.072, 0.097, 0.082, 0.067, 0.058, 0.081, 0.058]


#Dla Goal Bias 0.005
EST_005 =    [0.710, 0.611, 0.698, 0.719, 0.884, 0.762, 0.575, 0.972, 0.688, 0.915,0.873]
KPIECE_005 = [0.746,  0.953, 1.185,  1.000, 0.615, 1.614,  1.479, 1.779,  1.714,  1.400]
ProjEST_005 = [ 0.790,  0.497,  0.778, 0.560, 0.873,  0.764, 0.777,  0.833, 0.816, 0.969]
RRT_005 = [ 0.193, 0.107, 0.188, 0.269, 0.183, 0.240, 0.213, 0.170, 0.238, 0.255 ]
STRIDE_005 = [ 1.418, 0.973, 1.133, 1.200, 0.657,  0.741, 0.944,  0.875, 0.639, 0.842 ]
TRRT_005 = [ 0.311,  0.466,  0.253,  0.274, 0.237, 0.334, 0.297, 0.274, 0.384, 0.213 ]



EST1 = np.round(np.mean(EST_5), 3)
EST5 = np.round(np.mean(EST_05), 3)
EST20 = np.round(np.mean(EST_005), 3)

KPI1 = np.round(np.mean(KPIECE_5), 3)
KPI5 = np.round(np.mean(KPIECE_05), 3)
KPI20 = np.round(np.mean(KPIECE_005), 3)


Proj1 = np.round(np.mean(ProjEST_5), 3)
Proj5 = np.round(np.mean(ProjEST_05), 3)
Proj20 = np.round(np.mean(ProjEST_005), 3)

RRT1 = np.round(np.mean(RRT_5), 3)
RRT5 = np.round(np.mean(RRT_05), 3)
RRT20 = np.round(np.mean(RRT_005), 3)

STR1 = np.round(np.mean(STRIDE_5), 3)
STR5 = np.round(np.mean(STRIDE_05), 3)
STR20 = np.round(np.mean(STRIDE_005), 3)

TRRT1 = np.round(np.mean(TRRT_5), 3)
TRRT5 = np.round(np.mean(TRRT_05), 3)
TRRT20 = np.round(np.mean(TRRT_005), 3)




labels = ['EST', 'KPIECE', 'RRT', 'STRIDE', 'TRRT']
pla_1_means = [EST1, KPI1, RRT1, STR1, TRRT1]
pla_5_means = [EST5, KPI5, RRT5, STR5, TRRT5]
pla_20_means = [EST20, KPI20, RRT20, STR20, TRRT20]


x = np.arange(len(labels))  # the label locations
width = 0.14  # the width of the bars

# print(len(labels))
# print(len(pla_1_means))

fig, ax = plt.subplots()
ax.grid(zorder=0)
rects1 = ax.bar(x - width, pla_1_means, width, label='Goal bias: 0.5', color=['fuchsia'], zorder=3)
rects2 = ax.bar(x, pla_5_means, width, label='Goal bias: 0.05', color=['springgreen'],zorder=3 )
rects3 = ax.bar(x + width, pla_20_means, width, label='Goal bias: 0.005', color=['blue'], zorder=3 )

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Czas [s]')
ax.set_title('Porównanie czasów planowania dla różne zadanej dokładności (goal bias)')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()



def autolabel1(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


def autolabel2(rects):
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height+0.02),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

def autolabel3(rects):
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height+0.04),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel1(rects1)

# autolabel1([rects2[0], rects3[0], rects2[1], rects2[2], rects2[3],
#             rects2[4], rects3[4], rects2[5], rects3[5], rects2[9], rects3[9],
#             rects2[10], rects3[10], rects2[11], rects3[11], rects3[6], rects2[7]])


# autolabel2([rects3[7], rects3[1], rects3[2], rects3[3], rects2[6], rects2[8]])
# autolabel3([rects3[8]])
autolabel1(rects2)
autolabel1(rects3)

fig.tight_layout()


plt.show()
