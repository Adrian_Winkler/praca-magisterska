import matplotlib
import matplotlib.pyplot as plt
import numpy as np



CHOMP_1 = [0.313, 0.285,  0.263, 0.253, 0.255, 0.255, 0.259, 0.266, 0.264, 0.257, 0.265]
BFMT_1 = [1.023, 0.991, 1.007, 0.967, 0.996, 1.149, 1.003, 1.164, 0.959, 1.010]
BiEST_1 =  [0.037, 0.038, 0.042, 0.044, 0.038, 0.045, 0.042, 0.048, 0.047, 0.040]
BiTRRT_1 = [0.029, 0.021, 0.025, 0.021, 0.023, 0.022, 0.023, 0.020, 0.026, 0.025, 0.021]
EST_1 =    [0.095, 0.103, 0.101, 0.108, 0.144, 0.070, 0.144, 0.138, 0.119, 0.124, 0.185]
FMT_1 =    [0.903, 0.949, 0.965, 0.933, 0.910, 0.892, 0.892, 0.964, 0.936, 0.875, 0.953]
KPIECE_1 = [0.136, 0.139, 0.129, 0.172, 0.157, 0.270, 0.109, 0.134, 0.138, 0.111, 0.165, 0.197]
PDST_1 =   [0.180, 0.131, 0.148, 0.228, 0.133, 0.155, 0.117, 0.100, 0.137, 0.114, 0.071]
PRM_1 =   [0.087, 0.080, 0.100, 0.080, 0.087, 0.073, 0.082, 0.091, 0.057, 0.119]
ProjEST_1 = [0.136, 0.163, 0.087, 0.190, 0.123, 0.164, 0.105, 0.064, 0.156, 0.149, 0.197]
RRTConnect_1 = [0.037, 0.033, 0.036, 0.035, 0.033, 0.031, 0.022, 0.046, 0.049, 0.034]
RRT_1 = [0.052, 0.074, 0.050, 0.040, 0.080, 0.072, 0.046, 0.041, 0.037, 0.044, 0.032]
STRIDE_1 = [0.123, 0.107, 0.132, 0.092, 0.081, 0.150, 0.087, 0.125, 0.106, 0.175, 0.125]
TRRT_1 = [0.039, 0.034, 0.022, 0.047, 0.032, 0.035, 0.029, 0.030, 0.035, 0.030]
LIN_1 =  [0.007, 0.004, 0.006, 0.005, 0.007, 0.0004, 0.004, 0.006, 0.004, 0.006, 0.007, 0.005]
PTP_1 =  [0.001, 0.000, 0.001, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.001 ]


CHOMP_5 = [0.290, 0.284, 0.275, 0.272, 0.289, 0.288, 0.284, 0.279, 0.304, 0.273, 0.286]
BFMT_5 = [1.216, 1.272, 1.222, 1.378, 1.347, 1.206, 1.221, 1.224, 1.231, 1.404, 1.239]
BiEST_5 =  [0.045, 0.038, 0.036, 0.044, 0.034, 0.066, 0.037, 0.042, 0.038, 0.036]
BiTRRT_5 = [0.039, 0.042, 0.032, 0.038, 0.042, 0.038, 0.043, 0.053, 0.039, 0.037, 0.039]
EST_5 =    [0.078, 0.123, 0.098, 0.219, 0.170, 0.124, 0.094, 0.112, 0.163, 0.078, 0.081]
FMT_5 =    [2.512, 2.455, 2.751, 2.868, 2.551, 2.582, 2.842, 2.423, 2.614, 2.643, 2.460]
KPIECE_5 = [0.228, 0.292, 0.090, 0.082, 0.062, 0.241, 0.110, 0.111, 0.097, 0.108]
PDST_5 =   [0.121, 0.099, 0.113, 0.131, 0.073, 0.134, 0.081, 0.091, 0.119, 0.137, 0.171]
PRM_5 =   [0.083, 0.089, 0.115, 0.084, 0.098, 0.091, 0.085, 0.089, 0.071, 0.087]
ProjEST_5 = [0.101, 0.128, 0.105, 0.175, 0.078, 0.130, 0.125, 0.113, 0.098, 0.102]
RRTConnect_5 = [0.030, 0.030, 0.029, 0.032, 0.026, 0.027, 0.027, 0.033, 0.031, 0.029]
RRT_5 = [0.055, 0.075, 0.049, 0.066, 0.053, 0.068, 0.045, 0.057, 0.060, 0.057, 0.046]
STRIDE_5 = [0.099, 0.154, 0.079, 0.106, 0.138,  0.128, 0.169, 0.161, 0.100, 0.122]
TRRT_5 = [0.096, 0.068, 0.069, 0.072, 0.097, 0.082, 0.067, 0.058, 0.081, 0.058]
LIN_5=  [0.008, 0.009, 0.013, 0.010, 0.013, 0.011, 0.006, 0.011, 0.013, 0.010, 0.011]
PTP_5 =  [0.001, 0.000, 0.001, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.001] 


CH1 = np.round(np.mean(CHOMP_1), 3)
CH5 = np.round(np.mean(CHOMP_5), 3)

BF1 = np.round(np.mean(BFMT_1), 3)
BF5 = np.round(np.mean(BFMT_5), 3)

Bi1 = np.round(np.mean(BiEST_1), 3)
Bi5 = np.round(np.mean(BiEST_5), 3)

BiT1 = np.round(np.mean(BiTRRT_1), 3)
BiT5 = np.round(np.mean(BiTRRT_5), 3)

EST1 = np.round(np.mean(EST_1), 3)
EST5 = np.round(np.mean(EST_5), 3)

FMT1 = np.round(np.mean(FMT_1), 3)
FMT5 = np.round(np.mean(FMT_5), 3)

KPI1 = np.round(np.mean(KPIECE_1), 3)
KPI5 = np.round(np.mean(KPIECE_5), 3)

PRM1 = np.round(np.mean(PRM_1), 3)
PRM5 = np.round(np.mean(PRM_5), 3)

PDS1 = np.round(np.mean(PDST_1), 3)
PDS5 = np.round(np.mean(PDST_5), 3)

Pro1 = np.round(np.mean(ProjEST_1), 3)
Pro5 = np.round(np.mean(ProjEST_5), 3)

RRTC1 = np.round(np.mean(RRTConnect_1), 3)
RRTC5 = np.round(np.mean(RRTConnect_5), 3)

RRT1 = np.round(np.mean(RRT_1), 3)
RRT5 = np.round(np.mean(RRT_5), 3)

STR1 = np.round(np.mean(STRIDE_1), 3)
STR5 = np.round(np.mean(STRIDE_5), 3)

TRRT1 = np.round(np.mean(TRRT_1), 3)
TRRT5 = np.round(np.mean(TRRT_5), 3)

LIN1 = np.round(np.mean(LIN_1), 3)
LIN5 = np.round(np.mean(LIN_5), 3)

PTP1 = np.round(np.mean(PTP_1), 3)
PTP5 = np.round(np.mean(PTP_5), 3)



labels = ['CHOMP', 'BFMT', 'BiEST', 'BiTRRT', 'EST', 'FMT', 'KPIECE', 'PRM', 'PDST', 'ProjEST', 'RRTConnect', 'RRT', 'STRIDE', 'TRRT', 'LIN']
pla_1_means = [CH1, BF1, Bi1, BiT1, EST1, FMT1, KPI1, PRM1, PDS1, Pro1, RRTC1, RRT1, STR1, TRRT1, LIN1]
pla_5_means = [CH5, BF5, Bi5, BiT5, EST5, FMT5, KPI5, PRM5, PDS5, Pro5, RRTC5, RRT5, STR5, TRRT5, LIN5]

x = np.arange(len(labels))  # the label locations
width = 0.40  # the width of the bars

print(len(labels))
print(len(pla_1_means))

fig, ax = plt.subplots()
ax.grid(zorder=0)
rects1 = ax.bar(x - width/2, pla_1_means, width, label='1 złącze', color=['fuchsia'], zorder=3)
rects2 = ax.bar(x + width/2, pla_5_means, width, label='5 złącz', color=['springgreen'],zorder=3 )

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Czas [s]')
ax.set_title('Porównanie czasów planowania dla przypadków ruchu jednej i wszystkich osi')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()



def autolabel1(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    cnt = 0
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        cnt += 1


def autolabel2(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    cnt = 0
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height+0.05),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        cnt += 1




autolabel1(rects1)
autolabel2(rects2)

fig.tight_layout()


plt.show()
