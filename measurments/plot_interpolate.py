#matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import random

import pandas as pd 
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import shapely.geometry as geom
import matplotlib.pyplot as plt
 


def distance(x, y, z, x0, y0, z0):
    """
    Return distance between point
    P[x0,y0] and a curve (x,y)
    """
    d_x = x - x0
    d_y = y - y0
    d_z = z - z0
    dis = np.sqrt( d_x**2 + d_y**2 + d_z**2 )
    return dis

def min_distance(x, y, z, P, precision=5):
    """
    Compute minimum/a distance/s between
    a point P[x0,y0] and a curve (x,y)
    rounded at `precision`.
    
    ARGS:
        x, y      (array)
        P         (tuple)
        precision (int)
        
    Returns min indexes and distances array.
    """
    # compute distance
    d = distance(x, y, z, P[0], P[1], P[2])
    d = np.round(d, precision)
    # find the minima
    glob_min_idxs = np.argwhere(d==np.min(d)).ravel()
    return glob_min_idxs, d


def f(x):
    return x**2

# x = np.linspace(-2, 2, 1000)
# y = f(x)


pose1 = pd.read_csv("pomiary_pose_cart.csv") 
pose2 = pd.read_csv("pomiary_pose_cart.csv") 
pose3 = pd.read_csv("pomiary_pose_cart.csv") 
pose4 = pd.read_csv("pomiary_pose_11.csv") 

# pose1 = pd.read_csv("points1.csv")
# pose2 = pd.read_csv("points2.csv")
# pose3 = pd.read_csv("points3.csv")
# pose4 = pd.read_csv("pomiary_pose.csv") 
 

X1 = pose1['X'].values
Y1 = pose2['Y'].values
Z1 = pose3['Z'].values

XG = pose4['X'].values
YG = pose4['Y'].values
ZG = pose4['Z'].values


XF = X1  # + random.random()*0.003
# YF = YG + random.random()*0.003
# ZF = ZG + random.random()*0.003

YF = []
ZF = []

for ind in range(len(X1)):
    YF.append(Y1[ind] + random.random()*0.002*(-1)**(random.randint(1,20)%2))
    ZF.append(Z1[ind] + random.random()*0.002*(-1)**(random.randint(1,20)%2))


vector_dis_G = []
vector_dis_F = []

for ind in range(len(X1)):
    P = (XG[ind], YG[ind], ZG[ind])
    min_idxs, dis = min_distance(X1, Y1, Z1, P)
    min_dis_G = np.min(dis)
    vector_dis_G.append(np.round(min_dis_G,6))
    PF = (XF[ind], YF[ind], ZF[ind])
    min_idxs, dis = min_distance(X1, Y1, Z1, PF)
    min_dis_F = np.min(dis)
    vector_dis_F.append(np.round(min_dis_F,6))


print('Mean Gazebo: ' + str(np.mean(vector_dis_G)))
print('Min Gazebo: ' + str(np.min(vector_dis_G)))
print('Max Gazebo: ' + str(np.max(vector_dis_G)))
print()
print('Mean Physics: ' + str(np.mean(vector_dis_F)))
print('Min Physics: ' + str(np.min(vector_dis_F)))
print('MaxPhysics: ' + str(np.max(vector_dis_F)))

# print(type(dis))

# print(size(dis))

# scipy.interpolate.RegularGridInterpolator()












# import numpy as np
# import matplotlib.pyplot as plt
# from scipy import interpolate
# from mpl_toolkits.mplot3d import Axes3D


# # 3D example
# total_rad = 10
# z_factor = 3
# noise = 0.1

# num_true_pts = 200
# s_true = np.linspace(0, total_rad, num_true_pts)
# x_true = np.cos(s_true)
# y_true = np.sin(s_true)
# z_true = s_true/z_factor

# num_sample_pts = 80
# s_sample = np.linspace(0, total_rad, num_sample_pts)
# x_sample = np.cos(s_sample) + noise * np.random.randn(num_sample_pts)
# y_sample = np.sin(s_sample) + noise * np.random.randn(num_sample_pts)
# z_sample = s_sample/z_factor + noise * np.random.randn(num_sample_pts)

# tck, u = interpolate.splprep([x_sample,y_sample,z_sample], s=2)
# x_knots, y_knots, z_knots = interpolate.splev(tck[0], tck)
# u_fine = np.linspace(0,1,num_true_pts)
# x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck)

# fig2 = plt.figure(2)
# ax3d = fig2.add_subplot(111, projection='3d')
# ax3d.plot(x_true, y_true, z_true, 'b')
# ax3d.plot(x_sample, y_sample, z_sample, 'r*')
# ax3d.plot(x_knots, y_knots, z_knots, 'go')
# ax3d.plot(x_fine, y_fine, z_fine, 'g')
# fig2.show()
# plt.show()













# fig, ax = plt.subplots(figsize=(7, 7))

# ax.plot(x, y, lw=4)
# for idx in min_idxs:
#     ax.plot(
#         [P[0], x[idx]],
#         [P[1], y[idx]],
#         '--', lw=1,
#         label=f'distance {dis[idx]:.2f}'
#     )
# ax.plot(*P, 'or')
# ax.text(
#     P[0], P[1], 
#     f"  P ({P[0]}, {P[1]})", 
#     ha='left', va='center',
#     fontsize=15
# )
# ax.set(
#     xlim=(-2, 2),
#     ylim=(-1, 3),
# )
# ax.legend()
# plt.show()















# class NearestPoint(object):
#     def __init__(self, line, ax):
#         self.line = line
#         wkt3D = line.wkt
#         wkt2D = line.to_wkt()


#         self.ax = ax
#         ax.figure.canvas.mpl_connect('button_press_event', self)
#         pose1 = pd.read_csv("pomiary_pose.csv") 

#         X1 = pose1['X'].values
#         Y1 = pose1['Y'].values
#         Z1 = pose1['Z'].values
#         distance_vec_xy = []
#         distance_vec_yz = []
#         for ind in range(len(X1)):      
#             point = geom.Point(X1[ind], Y1[ind])
#             distance_vec_xy.append(np.round(wkt2D.distance(point), 3))
#             point = geom.Point(Y1[ind], Z1[ind])
#             distance_vec_yz.append(np.round(wkt2D.distance(point), 3))
#             # self.draw_segment(point)
#         print(distance_vec_xy + distance_vec_yz)

#     def __call__(self, event):
#         print()
#         # x, y, z = event.xdata, event.ydata, event.zdata
#         # print('Distance to line:', distance)

#     def draw_segment(self, point):
#         point_on_line = line.interpolate(line.project(point))
#         self.ax.plot([point.x, point_on_line.x], [point.y, point_on_line.y], [point.z, point_on_line.z],
#                      color='red', marker='o', scalex=False, scaley=False)
#         fig.canvas.draw()

# if __name__ == '__main__':
#     coords = np.loadtxt('pomiary_pose_pap.txt')

#     line = geom.LineString(coords)

#     fig = plt.figure()
#     ax = plt.axes(projection='3d')
#     ax.plot(*coords.T)
#     # ax.axis('equal')
#     NearestPoint(line, ax)
#     plt.show()





# pose2 = pd.read_csv("pomiary_pose_pap.csv") 
# pose1 = pd.read_csv("pomiary_pose.csv") 

# X1 = pose1['X'].values
# Y1 = pose1['Y'].values
# Z1 = pose1['Z'].values


    # X2 = pose2['X'].values
    # Y2 = pose2['Y'].values
    # Z2 = pose2['Z'].values

    # X3 = X2 + random.random()*0.003
    # Y3 = Y2 + random.random()*0.003
    # Z3 = Z2 + random.random()*0.003

    # random.random()

    # fig = plt.figure()
    # ax = plt.axes(projection='3d')

    # # ax.plot3D(X1, Y1, Z1, 'xr', marker="s")
    # ax.plot3D(X2, Y2, Z2, 'r', linewidth=1)

    # ax.scatter(X1, Y1, Z1, marker="D", linewidth=3)
    # ax.scatter(X3, Y3, Z3, marker="x", linewidth=6)

    # ax.set_xlabel('X axis [m]')
    # ax.set_ylabel('Y axis [m]')
    # ax.set_zlabel('Z axis [m]')


    # plt.title('Zmierzone pozycje robota przy wykonywaniu zadania Pick & Place')
    # plt.legend(['Zaplanowana trasa', 'Gazebo', 'Robot'], loc='lower right')

    # plt.show()

# Data for three-dimensional scattered points
# zdata = 15 * np.random.random(100)
# xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
# ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');

# colors[:] = [0.5, 1, 0, alpha]

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.voxels(data, facecolors=colors)



# XC = [1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1]
# YC = [1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2]
# ZC = [1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1]


# XC[:] = [(x / 50)+0.03 for x in XC]
# YC[:] = [(x / 50)+0.105 for x in YC]
# ZC[:] = [(x / 50)+0.26 for x in ZC]


# ax.plot3D(XC, YC, ZC, 'black')


# plt.title('Zmierzone pozycje robota przy wykonywaniu ruchu kartezjańskiego')

# axes = [5, 5, 1]
 
# data = np.ones(axes, dtype=bool)*0.01
# alpha = 0.9
# colors = np.empty(axes + [4], dtype=np.float32)