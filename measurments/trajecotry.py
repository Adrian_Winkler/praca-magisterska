#matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd 
from mpl_toolkits.mplot3d import Axes3D

pose1 = pd.read_csv("pomiary_pose1.csv") 
pose2 = pd.read_csv("pomiary_pose2.csv") 
pose3 = pd.read_csv("pomiary_pose3.csv") 

X1 = pose1['X'].values
Y1 = pose1['Y'].values
Z1 = pose1['Z'].values

X2 = pose2['X'].values
Y2 = pose2['Y'].values
Z2 = pose2['Z'].values

X3 = pose3['X'].values
Y3 = pose3['Y'].values
Z3 = pose3['Z'].values

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.plot3D(X1, Y1, Z1, 'red')
ax.plot3D(X2, Y2, Z2, 'blue')
ax.plot3D(X3, Y3, Z3, 'green')

ax.set_xlabel('X axis [mm]')
ax.set_ylabel('Y axis [mm]')
ax.set_zlabel('Z axis [mm]')

axes = [5, 5, 1]
 
data = np.ones(axes, dtype=bool)*0.01
alpha = 0.9
colors = np.empty(axes + [4], dtype=np.float32)
 

colors[:] = [0.5, 1, 0, alpha]

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.voxels(data, facecolors=colors)



XC = [1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1]
YC = [1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2]
ZC = [1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1]


XC[:] = [(x / 50)+0.03 for x in XC]
YC[:] = [(x / 50)+0.105 for x in YC]
ZC[:] = [(x / 50)+0.26 for x in ZC]


ax.plot3D(XC, YC, ZC, 'black')


plt.title('Zaplanowane trajektorie poruszania się końcówki ramienia przy omijaniu tej samej przeszkody')

plt.show()

# Data for three-dimensional scattered points
# zdata = 15 * np.random.random(100)
# xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
# ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');