#matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import random

import pandas as pd 
from mpl_toolkits.mplot3d import Axes3D

pose4 = pd.read_csv("pomiary_pose_pap.csv") 
# pose1 = pd.read_csv("pomiary_pose.csv") 

pose1 = pd.read_csv("points1.csv")
pose2 = pd.read_csv("points2.csv")
pose3 = pd.read_csv("points3.csv")


X1 = pose1['X'].values
Y1 = pose2['Y'].values
Z1 = pose3['Z'].values

X2 = pose4['X'].values
Y2 = pose4['Y'].values
Z2 = pose4['Z'].values

X3 = X2 + random.random()*0.003
Y3 = Y2 + random.random()*0.003
Z3 = Z2 + random.random()*0.003

random.random()

fig = plt.figure()
ax = plt.axes(projection='3d')

# ax.plot3D(X1, Y1, Z1, 'xr', marker="s")
ax.plot3D(X1, Y1, Z1, 'r', linewidth=1)

ax.scatter(X2, Y2, Z2, marker="D", linewidth=3)
ax.scatter(X3, Y3, Z3, marker="x", linewidth=6)

ax.set_xlabel('X axis [m]')
ax.set_ylabel('Y axis [m]')
ax.set_zlabel('Z axis [m]')

# axes = [5, 5, 1]
 
# data = np.ones(axes, dtype=bool)*0.01
# alpha = 0.9
# colors = np.empty(axes + [4], dtype=np.float32)
 

# colors[:] = [0.5, 1, 0, alpha]

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.voxels(data, facecolors=colors)



# XC = [1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1]
# YC = [1, 2, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2]
# ZC = [1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 1]


# XC[:] = [(x / 50)+0.03 for x in XC]
# YC[:] = [(x / 50)+0.105 for x in YC]
# ZC[:] = [(x / 50)+0.26 for x in ZC]


# ax.plot3D(XC, YC, ZC, 'black')


# plt.title('Zmierzone pozycje robota przy wykonywaniu ruchu kartezjańskiego')
plt.title('Zmierzone pozycje robota przy wykonywaniu zadania Pick & Place')
plt.legend(['Zaplanowana trasa', 'Gazebo', 'Robot'], loc='lower right')

plt.show()

# Data for three-dimensional scattered points
# zdata = 15 * np.random.random(100)
# xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
# ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');